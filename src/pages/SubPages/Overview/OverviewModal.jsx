import React from "react";

import { Button, Modal } from "react-bootstrap";
function OverviewModal(props) {
  return (
    <div>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            BULAN SORSOGON
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <iframe
              width="100%"
              height='400px'
              src="https://www.youtube.com/embed/Dej0RJkTx7E"
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowfullscreen
            ></iframe>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default OverviewModal;
