import React, { useState, useEffect, useRef } from "react";
import "./Overview.scss";
import { Card, Col, Row } from "react-bootstrap";
import VideoView from "./OverviewModal";
//
import playbutton from "../../../assets/icons/play-button.png";
import GmapBulan from "../../../assets/pics/Gmap-Bulan.jpg";
import MunicipyoMonum from "../../../assets/pics/monum.jpg";

function Overview() {
  const [showVideo, setShowVideo] = useState(false);

  const handlePlay = () => {
    setShowVideo(true);
  };

  const videoSrc = "https://www.youtube.com/watch?v=Dej0RJkTx7E";

  // useEffect(() => {
  //   const handleClickOutside = (event) => {
  //     if (videoRef.current && !videoRef.current.contains(event.target)) {
  //       setShowVideo(false);
  //     }
  //   };

  //   document.addEventListener("click", handleClickOutside);

  //   return () => {
  //     document.removeEventListener("click", handleClickOutside);
  //   };
  // }, [showVideo]);

  return (
    <div className="Overview-Page">
      <div className="Container-Overview">
        <Row className="Row-Header-Overview">
          <Col className="Col-Header-Overview">
            <div className="Title-Overview">
              <h1 className="Overview-h1">
                Overview of <span className="Overview-span">Bulan </span>
              </h1>
              <p className="span-bar"></p>
              <h3 className="Overview-h2">Unhan Bulan</h3>
            </div>
          </Col>
          <Col className="Col-Header-Overview">
            <div className="col-media" style={{ zIndex: "8" }}>
              <button
                className="Btn-play"
                onClick={handlePlay}
                style={{ zIndex: "5" }}
              >
                <img
                  className="btn-plany-icon"
                  src={playbutton}
                  alt="PlayButton"
                />
              </button>
              {showVideo && (
                // <div className="video-modal">
                //   <div className="video-div-container">
                //     <div className="video-wrapper">
                //       <iframe
                //         width="560"
                //         height="315"
                //         src="https://www.youtube.com/embed/Dej0RJkTx7E"
                //         title="YouTube video player"
                //         frameBorder="0"
                //         allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                //         allowFullscreen
                //       ></iframe>
                //     </div>
                //   </div>
                // </div>
                <VideoView
                  show={showVideo}
                  onHide={() => setShowVideo(false)}
                />
              )}
            </div>

            {/* iframe */}

            {/* pulsate svg */}
            <div className="pulsate-animation">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="100%"
                height="100%"
              >
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="5%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="10%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="15%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="20%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="25%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="30%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="35%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="40%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                {/* <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="45%"
                  fillOpacity=".1"
                  fill="#4194b3"
                />
                <circle
                  className="animate"
                  cx="50%"
                  cy="50%"
                  r="50%"
                  fillOpacity=".1"
                  fill="#4194b3"
                /> */}
              </svg>
            </div>
            {/*  */}
          </Col>
        </Row>

        <div className="custom-shape-divider-bottom-1682472927">
          <svg
            data-name="Layer 1"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 1200 120"
            preserveAspectRatio="none"
          >
            <path
              d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z"
              className="shape-fill"
            ></path>
          </svg>
        </div>
        {/*  */}
      </div>

      <div className="div-Overview-Geography">
        <Row xs={1} md={2}>
          <Col className="d-flex justify-content-center" xs={12} md={6}>
            <div className="Geography-Description">
              <h1>GEOGRAPHY</h1>
              <p>
                The Municipality of Bulan is located at the south-westernmost
                tip of the Bicol Peninsula of the island of Luzon. It has an
                area of exactly 20,094 hectares and is the terminal and
                burgeoning center of trade and commerce of its neighboring
                towns. It comprises fifty-five (55) barangays and eight (8)
                zones and is populated by people of diversified origin. This
                municipality is bounded on the north by the Municipality of
                Magallanes, on the east by the municipalities of Juban and
                Irosin, on the south by the Municipality of Matnog, and on the
                west by Ticao Pass. It has a distance of 667 kilometres (414 mi)
                from Manila, 63 kilometres (39 mi) from the province’s capital
                Sorsogon City, 20 kilometres (12 mi) from the town of Irosin and
                30 kilometres (19 mi) from the town of Matnog.
              </p>

              <h1>ECONOMY</h1>

              <p>
                Residents of Bulan are now looking forward to its cityhood
                because of its rapid-economic growth considering the fact that
                it is cited as the richest municipality in the province and 5th
                among the 1st class municipalities in Bicol Region with an
                average annual income of Php 58.8M. If it happens, Bulan will be
                the second city in the province and will be the 8th in the
                region. Major exports of this town are from its coastal waters,
                agricultural lands produce rice, copra, abaca fiber. Most of the
                revenues come from the fishing port of Bulan and businesses.
                There is one commercial bank in Bulan, the Philippine National
                Bank (with 2 ATMs), a rural bank Rural Bank of San Jacinto,
                Masbate, Camalig Bank, and the Producer’s Bank. There are also
                lending institutions like Intertrade, GSAC, and PALFSI that are
                very popular to SMEs.
              </p>
            </div>

            {/* <div>
              <h1>ECONOMY</h1>

              <p>
                Residents of Bulan are now looking forward to its cityhood
                because of its rapid-economic growth considering the fact that
                it is cited as the richest municipality in the province and 5th
                among the 1st class municipalities in Bicol Region with an
                average annual income of Php 58.8M. If it happens, Bulan will be
                the second city in the province and will be the 8th in the
                region. Major exports of this town are from its coastal waters,
                agricultural lands produce rice, copra, abaca fiber. Most of the
                revenues come from the fishing port of Bulan and businesses.
                There is one commercial bank in Bulan, the Philippine National
                Bank (with 2 ATMs), a rural bank Rural Bank of San Jacinto,
                Masbate, Camalig Bank, and the Producer’s Bank. There are also
                lending institutions like Intertrade, GSAC, and PALFSI that are
                very popular to SMEs.
              </p>
            </div> */}
          </Col>

          <Col xs={12} className="d-flex justify-content-center">
            <div>
              <Card className="Overview-Card shadow-sm">
                <Card.Img
                  className="Overview-Card-Image"
                  variant="top"
                  alt="Card image"
                  src={GmapBulan}
                />
                <Card.Body className="Card-Overview-Body"></Card.Body>
              </Card>

              <Card className="Overview-Card shadow-sm">
                <Card.Img
                  className="Overview-Card-Image"
                  variant="top"
                  alt="Card image"
                  src={MunicipyoMonum}
                />
                <Card.Body className="Card-Overview-Body"></Card.Body>
              </Card>
            </div>
          </Col>
        </Row>
      </div>

      <div className="div-Overview-Education">
        <div className="Overview-Education-content">
          <h1>EDUCATION</h1>

          <h3>Primary Education</h3>
          <p>
            The Primary Education in Bulan is divided into two Districts, the
            Bulan North District comprising all the Barangays towards the north
            coastal Barangays, to the interior land locked northern Barangays.
            The Bulan South District comprises the southern Coastal Barangays.
          </p>
          <h3>Secondary Education</h3>
          <p>
            Bulan has many secondary educational institutions. The largest
            public high school is Bulan National High School. Formerly, it was
            the Bulan High School/Bulan Vocational High School, before the
            former was converted into Sorsogon State College Bulan Campus. BNHS
            has satellite Campuses at Barangays Otavi, Beguin, J.P. Laurel, San
            Juan Bag-o. There is also a Secondary School in San Francisco, one
            of the biggest Barangay of Bulan. The San Franscisco National High
            School. On the coastal area, Quezon National High School,is one and
            only coastal high school in the area. Where students from nearby
            barangays (Osmeña, Aguinaldo, Sagrada and even Coron-Coron & Sua –
            part of Municipality of Matnog, use to send their students. The only
            school to represent major national events as headed by Mrs. Adelia
            O. Gregorio (Principal), Mr. Renato B. Gallenito and their
            co-teachers (by: HBF). There are various private secondary schools.
            The St. Louise De Marillac School (Formerly Colegio de la Inmaculada
            Concepcion) a Catholic school run by the Daughters of Charity
            religious congregation, Saint Bonaventure Academy of Butag, the
            Southern Luzon Institute-Kenerino Ramirez Asuncion Memorial School
            (SLI-KRAMS) were the oldest school in Bulan, Solis Institute of
            Technology, and A.G. Villaroya Technological Foundation Institute.
          </p>
          <h3>Tertiary Education</h3>
          <p>
            Sorsogon State University – Bulan Campus R.G. De Castro Colleges
            (formerly Quezon Academy) SLI-KRAMS Solis Institute of Technology
            A.G. Villaroya (Post-Secondary courses only) Alternative Learning
            System (non-formal education) Aside from the formal education
            system, a parallel alternative learning system program is
            incorporated in the education system to provide a viable alternative
            to the existing formal education structure. It encompasses both the
            non-formal and informal sources of knowledge and skills such as
            those acquired at home, the church, media, environment or even the
            life itself and span the pre-literacy to higher skills continuum.
            <br></br>
            <span>
              <b>There are two major existing programs implemented:</b>
            </span>
          </p>
          <ol>
            <li>Basic Literacy Program.</li>
            <li>
              Accreditation and Equivalency (A & E). ALS implementers such as
              Mobile Teachers and District ALS Coordinators were the one
              administer the implementation of the programs. It is intended for
              Out-of-School Youth and Adults who are unschooled or school
              drop-out. For more inquiries look for Bulan South and Bulan North
              Districts ALS implementers.
            </li>
          </ol>
        </div>
      </div>
    </div>
  );
}

export default Overview;
