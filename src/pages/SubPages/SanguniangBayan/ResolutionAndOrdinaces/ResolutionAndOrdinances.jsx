import React from 'react'
// import DownloadFile from '../../../../components/DownloadFile/DownloadFile'

const tabs = [
    {
      label: "Permits",
      key: "1",
      disabled: false,
      content: "Content of tab 1",
    },
    {
      label: "Forms",
      key: "2",
      disabled: false,
      content: "Content of tab 2",
    },
    // {
    //   label: "Tab 3",
    //   key: "3",
    //   disabled: true,
    //   content: "Content of tab 3",
    // },
  ];

function ResolutionAndOrdinances() {
  return (
    <div className='ResolutionAndOrdinances-Container'>
       <h1>DOWNLOADABLES</h1>
       <DownloadFile tabs={tabs} />
    </div>
  )
}

export default ResolutionAndOrdinances