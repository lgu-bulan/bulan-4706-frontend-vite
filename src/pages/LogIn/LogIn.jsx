import React, { useState, useEffect } from "react";
import { useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { AuthContext } from "../../context/authContext";
import "./LogIn.scss";
import { IoMailOutline, IoLockOpenOutline } from "react-icons/io5";

function LogIn() {
  
  //input
  const [inputs, setInputs] = useState({
    userName: "",
    password: "",
  });

  const [err, setError] = useState(null);

  const navigate = useNavigate();
  //useContext
  const { login, currentUser } = useContext(AuthContext);


  const [isActive, setIsActive] = useState(false);

  // button active or !
  useEffect(() => {
    const { userName, password } = inputs;
    if (userName !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [inputs]);

  const handleChange = (e) => {
    setInputs((prev) => ({ ...prev, [e.target.name]: e.target.value }));
    console.log(`test ${inputs.userName}, ${inputs.password}`);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    
    try {
      const response = await login(inputs);

      if (response.status === 200) {
        console.log('SUCCESS!');
        navigate("/DashBoard");
      } 
  

    } catch (err) {
      setError(err.response.data);

      if (err.response && err.response.status === 401) {
        console.log('Wrong credentials');
        setError(err.response.data.message);
      } 
      else if (err.response && err.response.status === 404){
        console.log('User Not Found');
        setError(err.response.data.message);
      }
      else {
        console.log('Unexpected error');
        setError('Unexpected error');
      }

    }


  };

  return (
    <section className="section--login--container">
      <form className="form--box" autoComplete="off" onSubmit={handleSubmit}>
        <div className="form--value">
          <h2 className="h2-LogIn">LogIn</h2>
          <div className="input--Box">
            <IoMailOutline className="ion-icon" />
            <input
              type="text"
              required
              placeholder="username"
              name="userName"
              value={inputs.userName}
              onChange={handleChange}
            />
            <label htmlFor="">Username</label>
          </div>

          

          <div className="input--Box">
            <IoLockOpenOutline className="ion-icon" />
            <input
              type="password"
              required
              placeholder="password"
              name="password"
              value={inputs.password}
              onChange={handleChange}
            />
            <label htmlFor="">Password</label>
          </div>
  
          <div className="register">
          {err && <p style={{ color: "red", margin:'5px'}}>* {err}</p>}
            {isActive ? (
              <button className="btn--LogIn">LogIn</button>
            ) : (
              <button className="btn--LogIn" disabled>
                LogIn
              </button>
            )}
          </div>
        </div>
      </form>
    </section>
  );

 
}

export default LogIn;
