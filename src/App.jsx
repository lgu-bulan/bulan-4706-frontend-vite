
import React, { useState } from "react";
import './App.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//
import Navbar from './components/Navbar/NavBar';
import Register from './pages/Register/Register';
import LogIn from './pages/LogIn/LogIn';
import SideBar from './components/SideBar/SideBar';
import Home from './pages/Home';
import DashBoard from './components/SideBar/SideBarContents/DashBoard/DashBoard';
import Upload from './components/upload/Upload';
import SingleUpload from "./components/upload/SingleUpload/SingleUpload";
import Upload2 from "./components/upload/Upload2";
import TestNavbar from './components/Navbar/NavBarTest';


// TEST
import Overview from './pages/SubPages/Overview/Overview';
import Resolutions from './pages/SubPages/SanguniangBayan/ResolutionAndOrdinaces/ResolutionAndOrdinances';


function App() {
  

  return (
    <Router>
        {/* <TestNavbar/>  */}
        {/* <LogIn /> */}
        {/* <SideBar /> */}
        {/* <Navbar/>  */}
        {/* <Overview /> */}
        <SingleUpload />
        <Upload />
        <Upload2 />
        
        {/* <Resolutions /> */}

        <Routes>
          {/* <Route path="/" element={<Home />} /> */}
          {/* <Route path="/DashBoard" element={<DashBoard />} /> */}
          <Route path="/LogIn" element={<LogIn />} />
          <Route path="/Register" element={<Register />} />
          <Route path="/Upload" element={<Upload />} />
        </Routes>
   
    </Router>
  );
}

export default App
