import axios from "axios";
import { createContext, useEffect, useState } from "react";
export const AuthContext = createContext()

export const AuthContexProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem("user") || null))
    
    const login = async (inputs) => {

        // const response =  await axios.post("http://localhost:5000/api/auth/LogIn", inputs)
        //     setCurrentUser(response.data)
        //     // console.log('\x1b[36m', response)
        //     // console.log('\x1b[36m', response.data)
        //     console.log('successful')
        //     return response;
     
            try {
                const response = await axios.post("http://localhost:5000/api/auth/LogIn", inputs);
                setCurrentUser(response.data);
                console.log(response);
                console.log(response.data);
                console.log('successful');
                return response;
              } catch (error) {
                console.error(error);
                throw new Error('Login failed');
              }
 };
    


    const logout = async (inputs) => {
        const res = await axios.post("/auth/logout");
        setCurrentUser(null);
    }

    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(currentUser));

    }, [currentUser]);

    return (
        <AuthContext.Provider value={{ currentUser, login, logout }}>
            {children}
        </AuthContext.Provider>
    )

}   