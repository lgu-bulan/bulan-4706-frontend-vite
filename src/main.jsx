import React from 'react'
import ReactDOM from 'react-dom/client'
import { AuthContexProvider } from "./context/authContext";
import App from './App'
import './index.css'


ReactDOM.createRoot(document.getElementById('root')).render(

    <React.StrictMode>
        <AuthContexProvider>
            <App />
        </AuthContexProvider>
    </React.StrictMode>

)
