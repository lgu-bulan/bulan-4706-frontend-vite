import React, { useState } from "react";
import "./NavBar.scss";

function Navbar() {
  // USESTATE FOR OPEN CLOSE MENU
  const [isOpen, setIsOpen] = useState(false);

  return (
    <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: salmon;">
    <div class="container-fluid">
      <a class="navbar-brand fw-bold" href="#">Coding Yaar</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Dropdown
            </a>
            <ul class="dropdown-menu">
              <li><a class="dropdown-item" href="#">Action</a></li>
              <li><a class="dropdown-item" href="#">Another action</a></li>
              <li>
                <hr class="dropdown-divider"/>
              </li>
              <li class="nav-item dropend">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li>
                    <hr class="dropdown-divider"/>
                  </li>
                  <li class="nav-item dropend">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      Dropdown
                    </a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="#">Action</a></li>
                      <li><a class="dropdown-item" href="#">Another action</a></li>
                      <li>
                        <hr class="dropdown-divider"/>
                      </li>
                      <li class="nav-item dropend">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Dropdown
                        </a>
                        <ul class="dropdown-menu">
                          <li><a class="dropdown-item" href="#">Action</a></li>
                          <li><a class="dropdown-item" href="#">Another action</a></li>
                          <li>
                            <hr class="dropdown-divider"/>
                          </li>
                          <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link">Link</a>
          </li>
        </ul>
        <form class="d-flex" role="search">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
          <button class="btn btn-outline-light" type="submit">Search</button>
        </form>
      </div>
    </div>
</nav>
    
    // <nav class="navbar navbar-expand-lg ">
    //   <div class="container-fluid div-navmain">
    //     <img
    //       className="mx-1"
    //       src="https://bulan4706.com/wp-content/uploads/2022/10/lgo.png"
    //       // margin="auto"
    //       width="67"
    //       height="67"
    //       // className="d-inline-block align-top"
    //       alt="React Bootstrap logo"
    //     />
    //     <a class="navbar-brand fw-bold" href="#">
    //       Bulan4706
    //     </a>
    //     <button
    //       class="navbar-toggler"
    //       type="button"
    //       data-bs-toggle="collapse"
    //       data-bs-target="#navbarSupportedContent"
    //       aria-controls="navbarSupportedContent"
    //       aria-expanded="false"
    //       aria-label="Toggle navigation"
    //     >
    //       <span class="navbar-toggler-icon"></span>
    //     </button>
    //     <div class="collapse navbar-collapse" id="navbarSupportedContent">
    //       <ul class="navbar-nav me-auto mb-2 mb-lg-0">
    //         <li class="nav-item">
    //           <a
    //             class="nav-link active"
    //             aria-current="page"
    //             href="#"
    //             style={{ color: "#fefefe" }}
    //           >
    //             Home
    //           </a>
    //         </li>

    //         {/* About Bulan Start */}
    //         <li class="nav-item dropdown">
    //           <a
    //             class="nav-link dropdown-toggle"
    //             href="#"
    //             role="button"
    //             data-bs-toggle="dropdown"
    //             aria-expanded="false"
    //           >
    //             About Bulan
    //           </a>
    //           <ul class="dropdown-menu">
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Overview
    //               </a>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li class="dropdown-item dropend">
    //               <a
    //                 class="nav-link dropdown-toggle custom--submenulink "
    //                 href="#"
    //                 role="button"
    //                 data-bs-toggle="dropdown"
    //                 aria-expanded="false"
    //               >
    //                 History
    //               </a>
    //               <ul class="dropdown-menu m-xs-2">
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Town
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Barangays
    //                   </a>
    //                 </li>
    //               </ul>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Tourism
    //               </a>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Business
    //               </a>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Town Gallery
    //               </a>
    //             </li>
    //           </ul>
    //         </li>
    //         {/* About Bulan End */}

    //         {/* Our LGU Start */}
    //         <li class="nav-item dropdown">
    //           <a
    //             class="nav-link dropdown-toggle"
    //             href="#"
    //             role="button"
    //             data-bs-toggle="dropdown"
    //             aria-expanded="false"
    //           >
    //             Our LGU
    //           </a>
    //           <ul class="dropdown-menu">
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Mission, Vision and <br></br>Quality Policy
    //               </a>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>

    //             {/*  */}
    //             <li class="dropdown-item dropend">
    //               <a
    //                 class="nav-link dropdown-toggle"
    //                 href="#"
    //                 role="button"
    //                 data-bs-toggle="dropdown"
    //                 aria-expanded="false"
    //               >
    //                 Office of the Mayor
    //               </a>
    //               <ul class="dropdown-menu">
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Mayor's Profile
    //                   </a>
    //                 </li>

    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Executive Orders
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>

    //                 <li class="nav-item dropend">
    //                   <a
    //                     class="nav-link dropdown-toggle"
    //                     href="#"
    //                     role="button"
    //                     data-bs-toggle="dropdown"
    //                     aria-expanded="false"
    //                   >
    //                     Executive and Legislative<br></br>Agenda
    //                   </a>
    //                   <ul class="dropdown-menu">
    //                     <li>
    //                       <a class="dropdown-item" href="#">
    //                         Comprehensive Land and<br></br>Use Plan
    //                       </a>
    //                     </li>
    //                   </ul>
    //                 </li>
    //               </ul>
    //             </li>
    //             {/*  */}

    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li class="dropdown-item dropend">
    //               <a
    //                 class="nav-link dropdown-toggle"
    //                 href="#"
    //                 role="button"
    //                 data-bs-toggle="dropdown"
    //                 aria-expanded="false"
    //               >
    //                 Sangguniang Bayan
    //               </a>
    //               <ul class="dropdown-menu">
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Members
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Resolution
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Ordinances
    //                   </a>
    //                 </li>
    //               </ul>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Government Services
    //               </a>
    //             </li>
    //           </ul>
    //         </li>
    //         {/* Our LGU End */}

    //         {/* News And Publications Start */}
    //         <li class="nav-item dropdown">
    //           <a
    //             class="nav-link dropdown-toggle"
    //             href="#"
    //             role="button"
    //             data-bs-toggle="dropdown"
    //             aria-expanded="false"
    //           >
    //             News & Publications
    //           </a>
    //           <ul class="dropdown-menu">
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 News
    //               </a>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Upcoming Events
    //               </a>
    //             </li>
    //           </ul>
    //         </li>
    //         {/*  News And Publications End */}

    //         {/* Transparency Start */}
    //         <li class="nav-item dropdown">
    //           <a
    //             class="nav-link dropdown-toggle"
    //             href="#"
    //             role="button"
    //             data-bs-toggle="dropdown"
    //             aria-expanded="false"
    //           >
    //             Transparency
    //           </a>
    //           <ul class="dropdown-menu">
    //             {/* Bids And Awards Submenu Start */}
    //             <li class="dropdown-item dropend">
    //               <a
    //                 class="nav-link dropdown-toggle"
    //                 href="#"
    //                 role="button"
    //                 data-bs-toggle="dropdown"
    //                 aria-expanded="false"
    //               >
    //                 Bids & Awards
    //               </a>
    //               <ul class="dropdown-menu">
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Invitation to Bid
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Request for Quotation
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Notice to Proceed
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Notice of Awards
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     Notice for Negotiated<br></br>Procurement
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <hr class="dropdown-divider" />
    //                 </li>
    //                 <li>
    //                   <a class="dropdown-item" href="#">
    //                     BAC Resolutions
    //                   </a>
    //                 </li>
    //               </ul>
    //             </li>
    //             {/* Bids And Awards Submenu End */}
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>

    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Annual Report
    //               </a>
    //             </li>
    //           </ul>
    //         </li>
    //         {/* Transparency End */}

    //         {/* E-Services - Start */}
    //         <li class="nav-item dropdown">
    //           <a
    //             class="nav-link dropdown-toggle"
    //             href="#"
    //             role="button"
    //             data-bs-toggle="dropdown"
    //             aria-expanded="false"
    //           >
    //             E-Services
    //           </a>
    //           <ul class="dropdown-menu">
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 iBPLS (Integrated Business <br></br>Processing and Licensing
    //                 System)
    //               </a>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Police Clearance
    //               </a>
    //             </li>
    //             <li>
    //               <hr class="dropdown-divider" />
    //             </li>
    //             <li>
    //               <a class="dropdown-item" href="#">
    //                 Downloadables
    //               </a>
    //             </li>
    //           </ul>
    //         </li>
    //         {/* E-Services - End */}

    //         <li class="nav-item">
    //           <a
    //             class="nav-link active"
    //             aria-current="page"
    //             href="#"
    //             style={{ color: "#fefefe" }}
    //           >
    //             E-Community
    //           </a>
    //         </li>
    //       </ul>
    //       {/* <form class="d-flex" role="search">
    //         <input
    //           class="form-control me-2"
    //           type="search"
    //           placeholder="Search"
    //           aria-label="Search"
    //         />
    //         <button class="btn btn-outline-light" type="submit">
    //           Search
    //         </button>
    //       </form> */}
    //     </div>
    //   </div>
    // </nav>
  );
}

export default Navbar;
