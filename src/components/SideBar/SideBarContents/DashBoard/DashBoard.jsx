import React, { useEffect, useState, useRef } from "react";
import "./DashBoard.scss";


function Dashboard({ status, handleSidebarToggle }) {
  // const [status, setStatus] = useState(
  //   () => localStorage.getItem("status") || "open"
  // );

  const sidebarRef = useRef(null);

  // useEffect(() => {
  //   sidebarRef.current.classList.toggle("close", status === "close");
  //   console.log(status);
  //   localStorage.setItem("status", status);
  // }, [status]);

  // const handleSidebarToggle = () => {
  //   setStatus(status === "open" ? "close" : "open");
  // };
  return (
    <>
      <div className="dash-content">
        <div className="overview">
          <div className="title">
            <i className="fa-solid fa-tachograph-digital"></i>
            <span className="text">Dashboard</span>
          </div>

          <div className="boxes">
            <div className="box box1">
              <i className="uil uil-thumbs-up"></i>
              <span className="text">Total Post</span>
              <span className="number">50,120</span>
            </div>
            <div className="box box2">
              <i className="uil uil-comments"></i>
              <span className="text">Comments</span>
              <span className="number">20,120</span>
            </div>
            <div className="box box3">
              <i className="uil uil-share"></i>
              <span className="text">Total Share</span>
              <span className="number">10,120</span>
            </div>
          </div>
        </div>

        <div className="activity">
          <div className="title">
            <i className="fa-solid fa-clock"></i>
            <span className="text">Recent Activity</span>
          </div>

          <div className="table-responsive">
            <table className="table table-secondary  table-striped">
              <thead>
                <tr>
                  <th scope="col">Acc. Name</th>
                  <th scope="col">Acc. Type</th>
                  <th scope="col">Date</th>
                  <th scope="col">Activity</th>
                </tr>
              </thead>
              <tbody className="table-group-divider">
                <tr>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                  <td>Liked</td>
                </tr>
                <tr>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>@fat</td>
                  <td>Liked</td>
                </tr>
                <tr>
                  <td colSpan="2">Larry the Bird</td>
                  <td>@twitter</td>
                  <td>Liked</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}

export default Dashboard;
