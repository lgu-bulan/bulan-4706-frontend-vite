import React from "react";
import { DashBoard } from "./SideBarImports";
import {
  BrowserRouter as Router,
  useLocation,
  Link,
  NavLink,
  Routes,
  Route,
} from "react-router-dom";
import {
  DesktopOutlined,
  TeamOutlined,
  FileOutlined,
  PieChartOutlined,
  UserOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  ReadOutlined
  
} from "@ant-design/icons";
import { Breadcrumb, Layout, Menu, theme, Button } from "antd";
import { useState } from "react";

function SideBar() {
  const { Header, Content, Footer, Sider } = Layout;

  //   [Section] USE LOCATION
  const location = useLocation();
  const { pathname } = location;
  const pathnames = pathname.split("/").filter((item) => item);
  const capatilize = (s) => s.charAt(0).toUpperCase() + s.slice(1);
  //   [Section] USE LOCATION END

  function getItem(label, key, icon, children) {
    return {
      key,
      icon,
      children,
      label,
    };
  }
  const items = [
    getItem(<Link to="/DashBoard">DashBoard</Link>, "1", <PieChartOutlined />),
    getItem("Option 2", "/option-2", <DesktopOutlined />),
    getItem("User", "/user", <UserOutlined />, [
      getItem("Tom", "/user/tom"),
      getItem("Bill", "/user/bill"),
      getItem("Alex", "/user/alex"),
    ]),
    getItem("Content", "/team", <ReadOutlined />, [
      getItem("View Posts", "/ViewPosts"),
      getItem("Write Post", "/WritePost"),
      getItem("Edit Post", "/Edit Post"),
    ]),
    getItem("Files", "/files", <FileOutlined />),
  ];
  //   const items = [
  //     getItem("Option 1", "1", <PieChartOutlined />),
  //     getItem("Option 2", "2", <DesktopOutlined />),
  //     getItem("User", "sub1", <UserOutlined />, [
  //       getItem("Tom", "3"),
  //       getItem("Bill", "4"),
  //       getItem("Alex", "5"),
  //     ]),
  //     getItem("Team", "sub2", <TeamOutlined />, [
  //       getItem("Team 1", "6"),
  //       getItem("Team 2", "8"),
  //     ]),
  //     getItem("Files", "9", <FileOutlined />),
  //   ];
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <div className="SideBar">
      <Layout
        style={{
          minHeight: "100vh",
        }}
      >
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
        >
          <div
            style={{
              height: 32,
              margin: 16,
              background: "rgba(255, 255, 255, 0.2)",
            }}
          />
          console.log(items)
          <Menu
            theme="dark"
            defaultSelectedKeys={["1"]}
            mode="inline"
            // items={items}
          >
            {items.map((item) =>
            // console.log(item)
              item.children ? (
                <Menu.SubMenu key={item.to} icon={item.icon} title={item.label}>
                  {item.children.map((childItem) => (
                    <Menu.Item key={childItem.to} icon={childItem.icon}>
                      <Link to={childItem.to}>{childItem.label}</Link>
                    </Menu.Item>
                  ))}
                </Menu.SubMenu>
              ) : (
                <Menu.Item key={item.to} icon={item.icon}>
                  <Link to={item.to}>{item.label}</Link>
                </Menu.Item>
              )
            )}
          </Menu>
        </Sider>
        {/* RightSide */}
        <Layout className="site-layout" style={{ border: "red 2px solid" }}>
          {/* Top */}
          <Header
            style={{
              padding: 0,
              background: colorBgContainer,
            }}
          >
            <Button
              type="text"
              icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
              onClick={() => setCollapsed(!collapsed)}
              style={{
                fontSize: "16px",
                width: 64,
                height: 64,
              }}
            />
          </Header>
          {/* Content body/ breadCrumbs */}
          <Content
            style={{
              margin: "0 16px",
              border: "orange 2px solid",
            }}
          >
            {/*  ROUTES */}
            {/* <Routes>
              <Route path="/" element={<div>Welcome to the homepage!</div>} />
              <Route path="/user" element={<div>User</div>} />
              <Route path="/user/bill" element={<div>Bill is a cat.</div>} />
              <Route path="/team" element={<div>Team</div>} />
              <Route path="/team/team1" element={<div>Team 1</div>} />
              <Route path="/team/team2" element={<div>Team 2</div>} />
              <Route path="/files" element={<div>Files</div>} />
            </Routes>
             ROUTES */}
            <Breadcrumb
              style={{
                margin: "16px 0",
              }}
            >
              {/* <Breadcrumb.Item>User</Breadcrumb.Item>
              <Breadcrumb.Item>Bill</Breadcrumb.Item> */}
              {/*  */}
              {pathnames.length > 0 ? (
                <Breadcrumb.Item>
                  <Link to="/DashBoard">DashBoard</Link>
                </Breadcrumb.Item>
              ) : (
                <Breadcrumb.Item>DashBoard</Breadcrumb.Item>
              )}
              {pathnames.map((name, index) => {
                const routeTo = `/${pathnames.slice(0, index + 1).join("/")}`;
                const isLast = index === pathnames.length - 1;
                return isLast ? (
                  <Breadcrumb.Item>{capatilize(name)}</Breadcrumb.Item>
                ) : (
                  <Breadcrumb.Item>
                    <Link to={`${routeTo}`}>{capatilize(name)}</Link>
                  </Breadcrumb.Item>
                );
              })}
              {/*  */}
            </Breadcrumb>
            {/* div content body */}
            {/* <div
              style={{
                padding: 24,
                minHeight: 400,
                background: colorBgContainer,
              }}
            >
              Bill is a cat.
            </div> */}
            <div
              style={{
                padding: 24,
                minHeight: 400,
                background: colorBgContainer,
              }}
            >
              {/* if things get double under check app if same component is in route */}
              <Routes>
                <Route path="/DashBoard" element={<DashBoard />} />
                {/* <Route path="/" element={<div>DASHBOARD!</div>} /> */}
                <Route path="/user" element={<div>ADD CONTENT</div>} />
                <Route path="/user/bill" element={<div>Bill is a cat.</div>} />
                <Route path="/team" element={<div>Team</div>} />
                <Route path="/team/team1" element={<div>Team 1</div>} />
                <Route path="/team/team2" element={<div>Team 2</div>} />
                <Route path="/files" element={<div>Files</div>} />
              </Routes>
              {/*  ROUTES */}
            </div>
          </Content>
          {/* Footer */}
          <Footer
            style={{
              textAlign: "center",
              border: "green 2px solid",
            }}
          >
            Ant Design ©2023 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    </div>
  );
}

export default SideBar;
