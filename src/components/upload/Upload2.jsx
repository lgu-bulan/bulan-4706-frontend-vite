import { Upload, message } from "antd";
import React, { useEffect, useRef, useState } from "react";
import "./Upload2.css";

//
const { Dragger } = Upload;

const Upload2 = () => {

  
  const props = {
    name: "file",
    listType: "picture",
    multiple: true,
    action: "http://localhost:5000/api/uploadTEST",
    onChange(info) {
      const { status } = info.file;
      // console.log(info.file.name);
      // console.log(info.file.name.length);
      let fileINFO = info.file;
      if (fileINFO) {
        let fileName = fileINFO.name;
        // console.log(
        //   `this is inside the ifelse ${fileName} and it's length ${fileName.length}`
        // );
        if (fileName.length >= 12) {
          let splitName = fileName.split(".");
          fileName = splitName[0].substring(0, 13) + "... ." + splitName[1];
          // console.log(fileName);
        }
      }
      if (status !== "uploading") {
        // console.log("test");
        // console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
  };

  // ARROW DOWN INDICATOR

  const [hasOverflowX, setHasOverflowX] = useState(false);
  const elementRef = useRef(null);

  useEffect(() => {
    const element = elementRef.current;
    if (element && element.offsetWidth < element.scrollWidth) {
      setHasOverflowX(true);
      console.log(`UMAPAW!!! ${hasOverflowX}`)
    } else {
      setHasOverflowX(false);
    }
  }, [hasOverflowX]);



  useEffect(() => {
    const element = elementRef.current;
    const handleResize = () => {
      if (element.offsetWidth < element.scrollWidth) {
        setHasOverflowX(true);
      } else {
        setHasOverflowX(false);
      }
    };
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className="div-main-upload">
      <div className="wrapper">
        <Dragger className="DraggerForm" {...props} ref={elementRef} >
          <p className="ant-upload-drag-icon">
            <i
              className="fas fa-cloud-upload-alt"
              style={{ fontSize: "2.5rem", color: "#6990F2" }}
            ></i>
          </p>
          <p
            className="ant-upload-text"
            style={{ fontSize: "1.3rem", color: "#6990F2" }}
          >
            Click or drag file to this area to upload
          </p>
          <div className='ant-upload-list .ant-upload-list-picture' ref={elementRef} style={{background:'black'}}>
          {hasOverflowX && <i className="fa-solid fa-circle-arrow-down" style={{textAlign:'center'}}></i>}
          </div>
          
        </Dragger>
      </div>
    </div>
  );
};
export default Upload2;
