import React, { useState } from "react";
import axios from "axios";

function Upload() {
  const [file, setFile] = useState(null);
  const [uploadUrl, setUploadUrl] = useState(null);

  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };

  const handleUpload = async () => {
    const formData = new FormData();
    formData.append("file", file);

    try {
      const response = await axios.post(
        "http://localhost:5000/api/uploadTEST",
        formData
      );
      console.log(response.data)
      setUploadUrl(response.data.message);
    } catch (err) {
      console.error(err);
    }
  };
  
  return (
    <div>
      <input type="file" onChange={handleFileChange} />
      <button onClick={handleUpload}>Upload</button>
      {uploadUrl && (
        <div>
          File uploaded to: <a href={uploadUrl}>{uploadUrl}</a>
        </div>
      )}
    </div>
  );
}

export default Upload;
